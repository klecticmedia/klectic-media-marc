# klectic-media_marc
This application is a subdomain to Klectic Media focused around the skills and musings of Marc Ferguson. The design is based on the Wordpress template used for http://katina.klecticmedia.com.

## Install & Usage
```
npm install && npm start
```

## Experiment
This app is primarily built with `Bootstrap v4`.
