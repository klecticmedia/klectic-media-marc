import React, { Component } from "react";
import { Link } from "react-router-dom";

class Home extends Component {
  componentDidMount() {
    if ($) {
      $("[data-toggle='tooltip']").tooltip();
    }
  }

  renderSidebar() {
    return (
      <div className="sidebar mt-4 ml-4 mb-4">
        <section>
          <h3>Articles</h3>
          <ul className="list-group">
            <li className="list-group-item">
              <Link to="/surviving-modern-javascript-development">Surviving Modern JavaScript Development</Link>
            </li>
          </ul>
        </section>
      </div>
    );
  }

  render() {
    return (
      <div className="container page">
        <div className="content">
          <div className="row">
            <div className="col-sm-12 col-md-8 order-1 order-xs-2">
              <div className="row mt-5 border-bottom">
                <div className="col-xs-12 col-sm-4 col-md-5 text-center">
                  <img src="/images/marc_400.jpg" className="img-fluid rounded-circle" />
                </div>
                <div className="col-xs-12 col-sm-8 col-md-7">
                  <h2>&Agrave; propos</h2>
                  <p>At my core, I’m an open source front-end web developer from a galaxy far, far away. When I’m not fighting imperial regimes I fall into my role as a mild-mannered husband raising three titans-of-industry. A busy and productive life often requires the counter balance of slothful recreations. For fun I pwn noobz, farm resources, and plays-the-objective, which results in a higher state of “gamerscore.”</p>
                  <p>When it comes to technology I’ve always had a very deep relationship with her. We can just sit and talk for hours. I’ll tell her secrets and she’ll reveal some to me.</p>
                </div>
              </div>
              <h2 className="mt-2">For Fun</h2>
              <ul data-toggle="tooltip" title="In case you're wondering, yes I copied this same format from my wife.">
                <li>I dont read a lot of books, but I tend to read more about implementing libraries and frameworks.</li>
                <li>I attend tabletop roleplaying meetups, specifically for Pathfinder RPG and Dungeons & Dragons.</li>
                <li>Some of my favorite podcasts are Frontend Happy Hour, Anything from Jupiter Broadcasting, and Wait, Wait Dont Tell Me (from NPR).</li>
              </ul>
            </div>
            <div className="col-sm-12 col-md-4 order-2 order-xs-1">
              { this.renderSidebar() }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
