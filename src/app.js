import React from "react";
import { Route } from "react-router-dom";

// components
import Footer from "./components/footer";
import Header from "./components/header";

// pages
import Home from "./pages/home";
import Article from "./pages/article";

import "./stylesheets/main.scss";

const App = () => {
  return (
    <div>
      <Header />
        <Route path="/" exact component={Home} />
        <Route path="/:slug" component={Article} />
      <Footer />
    </div>
  );
}

export default App;
