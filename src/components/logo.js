import React from 'react';
import PropTypes from 'prop-types';

const Logo = (props) => {
  const inverseClass = props.inverse ? 'inverse' : '';
  const headerClass = props.header ? 'header' : '';
  return (
    <div className={`container logo-wrapper ${headerClass} ${inverseClass}`}>
      <div className='logo-container'>
        <span className='title'>Marc Codes</span>
        <div className="background-bar">
          <span className='subtitle'>Articles, Apps, and more</span>
        </div>
      </div>
    </div>
  );
}

Logo.propTypes = {
  header: PropTypes.bool,
  inverse: PropTypes.bool
}

export default Logo;
