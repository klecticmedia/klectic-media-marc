import React from "react";

const renderIcon = (icon, url) => {
  return (
    <div className="fa-container d-inline-block mr-2" key={icon}>
      <a href={url} rel="noopener noreferrer" title={icon} target="_blank">
        <i className={`fab fa-${icon} fa-lg`}></i>
      </a>
    </div>
  );
}

const renderIcons = () => {
  const data = [
    {
      "icon": "codepen",
      "url": "https://codepen.io/fergatROn"
    },
    {
      "icon": "facebook",
      "url": "https://facebook.com/marc.ferguson"
    },
    {
      "icon": "github",
      "url": "https://github.com/fergatron"
    },
    {
      "icon": "linkedin",
      "url": "https://linkedin.com/in/marcfferguson"
    },
    {
      "icon": "stack-overflow",
      "url": "https://stackoverflow.com/users/843174/fergatron"
    },
    {
      "icon": "twitter",
      "url": "https://twitter.com/fergatron"
    }
  ];

  return data.map((d) => {
    return renderIcon(d.icon, d.url);
  });
}

const SocialBar = () => {
  return (
    <div className="social-bar">
      { renderIcons() }
    </div>
  );
}

export default SocialBar;
