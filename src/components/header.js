import React from "react";
import { Link } from "react-router-dom";

import Logo from "./logo";
import SocialBar from "./social-bar";

const Navigation = () => {
  return (
    <nav className="container navbar navbar-expand-lg navbar-dark bg-dark">
    <button
      className="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarToggler"
      aria-controls="navbarToggler"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarToggler">
      <ul className="navbar-nav d-flex align-items-center">
        <li className="nav-item">
          <Link className="nav-link" to="/">About</Link>
        </li>
        <li className="nav-item ml-md-auto">
          <SocialBar />
        </li>
      </ul>
    </div>
    </nav>
  );
}

const Header = () => {
  return (
    <div>
      <header>
        <div className="nav-wrapper">
          <div className="nav-container">
            { Navigation() }
          </div>
        </div>
        <Logo header />
      </header>
    </div>
  );
}

export default Header;
