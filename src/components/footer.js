import React from "react";
import { Link } from "react-router-dom";

import Logo from "./logo";
import SocialBar from "./social-bar";

const renderNav = () => {
  return (
    <ul className="nav">
      <li className="nav-item">
        <Link className="nav-link" to="/">About</Link>
      </li>
    </ul>
  );
}

const Footer = () => {
  return (
    <div className="footer-wrapper">
      <footer className="container">
        <div className="row align-items-center border border-dark pt-2 pb-2">
          <div className="col-xs-12 col-md-8">
            { renderNav() }
          </div>
          <div className="col-xs-12 col-md-4">
            <div className="float-right">
              <SocialBar />
            </div>
          </div>
        </div>
        <div className="row border border-dark pt-4 pb-5">
          <div className="col-xs-12 col-md-6">
            <span className="copyright">
              &copy; 2018 Klectic Media, LLC. All rights reserved.
            </span>
          </div>
          <div className="col-xs-12 col-md-6">
            <div className="mx-auto">
              <Logo inverse />
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}

export default Footer;
